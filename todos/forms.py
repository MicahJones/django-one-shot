

from django import forms

from todos.models import TodoList




class TodoListForm(forms.ModelForm):
    class Meta:
        model = TodoList
        fields = ['name']  # Include all fields you want to be in the form
        # name = forms.CharField(max_length=100),
      
